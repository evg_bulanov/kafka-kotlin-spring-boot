## Kafka Kotlin Spring Boot Demo application

It's a demo application of kafka, kotlin and spring boot, based on article of @Piotr Wolak
(thanks for startup article at https://codersee.com/apache-kafka-with-spring-boot-and-kotlin/).

## Case of demo project

We have a simple medical system that prescribe a medicine for a patient.
The first part consist of two REST services: create patient, create (prescribe) medicine for a patient
It handles requests and stores messages in kafka. 




