package com.jk.kafka.demo.producer

import com.jk.kafka.demo.config.MEDICINE_PRESCRIBE
import com.jk.kafka.demo.model.MedicineKafkaRequest
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class MedicineKafkaProducer (private val medicineKafkaTemplate: KafkaTemplate<String, MedicineKafkaRequest>) {
    fun sendKafkaPrescribeMedicine(patientId: String, code: String, name: String) {
        medicineKafkaTemplate.send(MEDICINE_PRESCRIBE, "$patientId:$code", MedicineKafkaRequest(patientId, code, name))
    }
}