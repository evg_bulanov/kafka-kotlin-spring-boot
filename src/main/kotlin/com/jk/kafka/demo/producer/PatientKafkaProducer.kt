package com.jk.kafka.demo.producer

import com.jk.kafka.demo.config.PATIENT_CREATE
import com.jk.kafka.demo.model.PatientKafkaRequest
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class PatientKafkaProducer (private val patientKafkaTemplate: KafkaTemplate<String, PatientKafkaRequest>){
    fun sentKafkaCreatePatient(id: String, name: String, birthDate: LocalDate) {
        patientKafkaTemplate.send(PATIENT_CREATE, id, PatientKafkaRequest(id, name, birthDate))
    }
}