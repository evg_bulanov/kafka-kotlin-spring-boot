package com.jk.kafka.demo.repository

import com.jk.kafka.demo.model.MedicineEntity
import com.jk.kafka.demo.model.PatientEntity
import org.springframework.data.jpa.repository.JpaRepository

interface PatientRepository: JpaRepository<PatientEntity, String>
interface MedicineRepository: JpaRepository<MedicineEntity, String>
