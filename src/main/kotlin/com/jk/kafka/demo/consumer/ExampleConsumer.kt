package com.jk.kafka.demo.consumer

import com.jk.kafka.demo.config.MEDICINE_CONSUMER_GROUP
import com.jk.kafka.demo.config.MEDICINE_PRESCRIBE
import com.jk.kafka.demo.config.PATIENT_CONSUMER_GROUP
import com.jk.kafka.demo.config.PATIENT_CREATE
import com.jk.kafka.demo.model.MedicineEntity
import com.jk.kafka.demo.model.MedicineKafkaRequest
import com.jk.kafka.demo.model.PatientEntity
import com.jk.kafka.demo.model.PatientKafkaRequest
import com.jk.kafka.demo.repository.MedicineRepository
import com.jk.kafka.demo.repository.PatientRepository
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component
import java.util.UUID

@Component
class ExampleConsumer (val medicineRepository: MedicineRepository, val patientRepository: PatientRepository){
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @KafkaListener(topics = [PATIENT_CREATE], groupId = PATIENT_CONSUMER_GROUP)
    fun createPatientListener(message: PatientKafkaRequest) {
        logger.info("Message received: [$message]")
        val patient = patientRepository.findById(message.id)
        if (!patient.isPresent) {
            logger.info("Save patient: ${message.id}")
            patientRepository.save(PatientEntity(message.id, message.name, message.birthDate))
        }
    }

    @KafkaListener(topics = [MEDICINE_PRESCRIBE], groupId = MEDICINE_CONSUMER_GROUP)
    fun prescribeMedicineListener(message: MedicineKafkaRequest) {
        logger.info("Message received: [$message]")
        val patient = patientRepository.findById(message.patientId)
        if (!patient.isPresent) {
            logger.info("Patient is not found! medicine ${message.code} could not be prescribed")
        } else {
            logger.info("Save medicine ${message.code} for patient ${message.patientId}")
            val medicineEntity =
                MedicineEntity(UUID.randomUUID().toString(), message.patientId, message.code, message.name)
            medicineRepository.save(medicineEntity)
        }
    }
}