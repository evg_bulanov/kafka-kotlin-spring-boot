package com.jk.kafka.demo.common

import java.time.LocalDate
import kotlin.random.Random

class Utils {
    companion object {
        fun randomString(length: Int): String {
            val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
            return (1..length)
                .map { allowedChars.random() }
                .joinToString("")
        }

        fun randomDate(): LocalDate {
            return LocalDate.now().minusDays(Random.nextLong(0, 1000))
        }
    }
}