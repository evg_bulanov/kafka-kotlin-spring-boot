package com.jk.kafka.demo.controller

import com.jk.kafka.demo.model.MedicineDTO
import com.jk.kafka.demo.producer.MedicineKafkaProducer
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class MedicineController (private val medicineKafkaProducer: MedicineKafkaProducer) {
    @PostMapping("/medicine/{patientId}/prescribe")
    fun prescribeMedicine(@PathVariable("patientId") patientId: String, @RequestBody medicine: MedicineDTO) {
        medicineKafkaProducer.sendKafkaPrescribeMedicine(patientId, medicine.code, medicine.name)
    }
}