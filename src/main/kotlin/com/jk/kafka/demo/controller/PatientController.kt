package com.jk.kafka.demo.controller

import com.jk.kafka.demo.model.PatientDTO
import com.jk.kafka.demo.producer.PatientKafkaProducer
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class PatientController (private val patientProducer: PatientKafkaProducer) {

    @PostMapping("/patient")
    fun createPatient(@RequestBody dto: PatientDTO){
        patientProducer.sentKafkaCreatePatient(dto.id, dto.name, dto.birthDate);
    }
}