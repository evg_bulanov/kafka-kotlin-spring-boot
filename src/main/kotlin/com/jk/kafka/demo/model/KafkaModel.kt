package com.jk.kafka.demo.model

import java.time.LocalDate

data class PatientKafkaRequest (val id: String, val name: String, val birthDate: LocalDate)
data class MedicineKafkaRequest (val patientId: String, val code: String, val name: String)