package com.jk.kafka.demo.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.LocalDate

@Table(name = "patient")
@Entity
class PatientEntity (
    @Id
    @Column(name = "id", nullable = false)
    var id: String? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "birth_date", nullable = false)
    var birthDate: LocalDate? = null
)

@Table(name = "medicine")
@Entity
class MedicineEntity (
    @Id
    @Column(name = "id", nullable = false)
    var id: String? = null,

    @Column(name = "patient_id", nullable = false)
    var patientId: String? = null,

    @Column(name = "code", nullable = false)
    var code: String? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null
)