package com.jk.kafka.demo.config

const val PATIENT_CONSUMER_GROUP= "patient.group"
const val PATIENT_CREATE = "patient.create"

const val MEDICINE_CONSUMER_GROUP = "medicine.group"
const val MEDICINE_PRESCRIBE = "medicine.prescribe"

