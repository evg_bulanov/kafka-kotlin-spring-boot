package com.jk.kafka.demo.generator

import org.springframework.context.ApplicationEvent

class PatientGeneratedEvent(source: Any, val patientId: String) : ApplicationEvent(source)
// https://www.baeldung.com/spring-events
