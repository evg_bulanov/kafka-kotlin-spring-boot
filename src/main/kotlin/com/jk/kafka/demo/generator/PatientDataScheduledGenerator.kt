package com.jk.kafka.demo.generator

import com.jk.kafka.demo.common.Utils
import com.jk.kafka.demo.model.PatientDTO
import org.springframework.context.ApplicationEventPublisher
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.util.UUID

@Component
class PatientDataScheduledGenerator(private val restTemplate: RestTemplate,
                                    private val applicationEventPublisher: ApplicationEventPublisher) {
    @Scheduled(fixedDelay = 5_000)
    fun generatePatientEvery5Seconds() {
        val patient = PatientDTO(UUID.randomUUID().toString(), Utils.randomString(25), Utils.randomDate())
        var result = restTemplate.postForObject("http://localhost:8090/patient", patient, String::class.java)
        applicationEventPublisher.publishEvent(PatientGeneratedEvent(this, patient.id))
    }
}