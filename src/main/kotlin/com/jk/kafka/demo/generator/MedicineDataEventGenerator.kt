package com.jk.kafka.demo.generator

import com.jk.kafka.demo.common.Utils
import com.jk.kafka.demo.model.MedicineDTO
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class MedicineDataEventGenerator (private val restTemplate: RestTemplate)
    : ApplicationListener<PatientGeneratedEvent> {

    override fun onApplicationEvent(event: PatientGeneratedEvent) {
        (1..10).forEach {
            Thread {
                val medicine = MedicineDTO(Utils.randomString(3), Utils.randomString(15))
                var result = restTemplate.postForObject(
                    "http://localhost:8090/medicine/${event.patientId}/prescribe",
                    medicine,
                    String::class.java
                )
            }.start()
        }
    }
}