CREATE TABLE patient (
    id VARCHAR(250) NOT NULL,
    name VARCHAR(250) NOT NULL,
    birth_date DATE NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX patient_pk_idx ON patient (id);
CREATE UNIQUE INDEX patient_name_idx ON patient (name);

CREATE TABLE medicine (
    id VARCHAR(250) NOT NULL,
    code VARCHAR(50) NOT NULL,
    name VARCHAR(250) NOT NULL,
    patient_id VARCHAR(250) NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX medicine_pk_idx ON medicine (id);
CREATE UNIQUE INDEX medicine_code_idx ON medicine (code);
CREATE INDEX medicine_pk_fk_idx ON medicine (patient_id);